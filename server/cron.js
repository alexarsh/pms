/**
 * Created by idan on 19/07/2015.
 */

var last_year = new Date();
last_year.setFullYear(last_year.getFullYear(), last_year.getMonth() - 12);
var curr_year = new Date();

SyncedCron.add({
    name: 'searching movies and writing to database',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 1 minute');
    },
    job: function() {
        var data=HTTP.get("https://api.themoviedb.org/3/discover/movie?api_key=28948f17f0b96cce3977231ef324225f", { //Get num of pages
            params: {
                "primary_release_date.gte": last_year,
                "primary_release_date.lte": curr_year,
            }
        });
        var content = JSON.parse(data["content"]);
        var total_pages=content["total_pages"];

        for (p=1; p<=total_pages;p++) {
            var data = HTTP.get("https://api.themoviedb.org/3/discover/movie?api_key=28948f17f0b96cce3977231ef324225f", {
                params: {
                    "primary_release_date.gte": last_year,
                    "primary_release_date.lte": curr_year,
                    "page": p
                }
            });
            var content = JSON.parse(data["content"]);
            for (i=0;i<content["results"].length;i++) {
                var id = (content["results"][i]["id"]);

                var data2 = HTTP.get("http://www.omdbapi.com/?r=json", {
                    params: {
                        "t": content["results"][i]["title"]
                    }
                });
                var content2 = JSON.parse(data2["content"]);

                var data3 = HTTP.get("https://yts.to/api/v2/list_movies.json?minimum_rating=0", {
                    params: {
                        "query_term": content["results"][i]["title"]
                    }
                });
                var content3 = JSON.parse(data3["content"]);

                var trailer=HTTP.get("http://trailersapi.com/trailers.json", {
                    params: {
                        "movie": content["results"][i]["title"]
                    }
                });
                trailer=JSON.parse(trailer["content"]);

                var dash_title=content["results"][i]["title"].replace(" ","-");
                var s_api=("http://subsmax.com/api/1/".concat(dash_title)).concat("-hebrew");
                var subtitles=HTTP.get(s_api, {
                });
                var s_arr=subtitles["content"].split("</link>",2);
                var s_url = (s_arr[1].split("<link>"))[1];
                if (s_url!=undefined) {
                    s_id = (s_url.split("/"))[5];
                    obj["subtitles"] = "http://subsmax.com/subtitles-movie/".concat(dash_title).concat("-hebrew/").concat("download-subtitle/").concat(s_id);
                }
                
                var obj={
                    "movie_id": id,
                    "title": content["results"][i]["title"],
                    "release_date": content["results"][i]["release_date"],
                    "overview": content["results"][i]["overview"]
                };

                if (content2["Response"]!="False") {
                    var genre_arr = (content2["Genre"]).split(",");
                    obj["imdbRating"] = parseFloat(content2["imdbRating"]) ? parseInt(content2["imdbRating"][0]) : 0;
                    obj["Genre1"] = genre_arr[0];
                    obj["Genre2"] = genre_arr[1];
                    obj["Genre3"] = genre_arr[2];
                }

                if (content3["data"]["movies"].length!=0) {
                    obj["torrents"] = content3["data"]["movies"][0]["torrents"];
                    obj["image"] = content3["data"]["movies"][0]["medium_cover_image"];
                }

                if (trailer!='') {
                    obj["trailer"]=trailer;
                }

                movies.upsert({"movie_id": id}, obj);
                console.log(content["results"][i]["title"])

            }
        }
    }
});
SyncedCron.start();