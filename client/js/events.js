var filters = {}, filtered_movies;
var set_movies = function() {
    filtered_movies = movies.find({torrents: {$exists: true}}).fetch();
    filters = Session.get("filters") || filters;

    function f(m) {
        if (filters["imdb_rating"] ) {
            return m.imdbRating >= filters["imdb_rating"];
        }
        else {
            return true;
        }
    }
    var filtered_movies = filtered_movies.filter(f);

    Session.set("movies", filtered_movies);

}

Template.home.events({
    "click ul#from a": function(e) {
        $(e.target).parent().parent().siblings().text($(e.target).text());
        $(e.target).parent().parent().siblings().val($(e.target).text());
        //Meteor.users.update(Meteor.userId(), {$set: {"quality_from": $(e.target).text()}});
        filters = Session.get("filters");
        filters["quality_from"] =  $(e.target).text();
        Session.set("filters", filters);
        set_movies();
    },
    "click ul#to a": function(e) {
        $(e.target).parent().parent().siblings().text($(e.target).text());
        $(e.target).parent().parent().siblings().val($(e.target).text());
        //Meteor.users.update(Meteor.userId(), {$set: {"quality_to": $(e.target).text()}});
        filters = Session.get("filters");
        filters["quality_to"] =  $(e.target).text();
        Session.set("filters", filters);
        set_movies();
    },
    "click #imdb_rating button": function(e) {
        $(e.target).addClass("active").siblings().removeClass("active");
        //Meteor.users.update(Meteor.userId(), {$set: {"imdb_rating": $(e.target).text()}});
        filters = Session.get("filters") || filters;
        filters["imdb_rating"] =  $(e.target).text();
        Session.set("filters", filters);
        set_movies();
    },
    "click #movie_type #movies": function(e) {
        if (document.getElementById("movies").checked)
            Meteor.users.update(Meteor.userId(), {$set: {"isMovie": "True"}});
        else Meteor.users.update(Meteor.userId(), {$set: {"isMovie": "False"}});
    },
    "click #movie_type #TV": function(e) {
        if (document.getElementById("TV").checked)
            Meteor.users.update(Meteor.userId(), {$set: {"isTV": "True"}});
        else Meteor.users.update(Meteor.userId(), {$set: {"isTV": "False"}});
    },
    "click #genres input[type=checkbox]": function(e) {
        var genres_arr = [];
        var checked=$("#genres input[type=checkbox]:checked");
        checked.each(function(index, value){
            genres_arr.push(value.id)
        });
        //Meteor.users.update(Meteor.userId(), {$set: {"genres": genres_arr}});
        filters = Session.get("filters");
        filters["genres"] =  genres_arr;
        Session.set("filters", filters);
        set_movies();
    },
    "click .btn-info": function(e){
        var mid = $(e.target).parent().parent().attr('id');
        var mov = movies.findOne({'_id':mid});
        Session.set('info', mov);
        if (mov.trailer) {
            $('#trailer_frame').attr('src', $(mov.trailer[0].code).attr('src')).show();
        } else {
            $('#trailer_frame').hide();
        }
    }
});

Template.home.onCreated(function () {
    set_movies();
});

Template.home.helpers({
    movies: function () {
        return Session.get("movies");
    }
});
