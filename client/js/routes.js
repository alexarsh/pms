//my_movies = Meteor.subscribe('movies', function() {
//    Session.set("movies",  movies.find().fetch());
//    return movies.find();
//});

Router.route('/', function () {
    this.render('home', {
        data: {"movies": movies.find({torrents: {$exists: true}}),
            "genres": ["Action", "Adventure", "Animation", "Biography", "Comedy", "Crime", "Documentary", "Drama",
                "Family", "Fantasy", "Film-Noir", "History", "Horror", "Music", "Musical", "Mystery", "Romance",
                "Sci-fi", "Sport", "Thriller", "War", "Western"]}
    });
});

Router.route('/info', function () {
    this.render('info');
});